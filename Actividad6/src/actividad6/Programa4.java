package actividad6;

import java.util.Stack;

public class Programa4 {
public static void main(String[] args) {
        
        Stack <String>  p1 = new Stack <String>();
        Stack <String>  p2 = new Stack <String>();
        Stack <String>  paux = new Stack <String>();
        Stack <String>  p3 = new Stack <String>();
        
        p1.push(Integer.toString(4));
        p1.push(Integer.toString(6));
        p1.push(Integer.toString(3));
        p2.push(Integer.toString(30));
        p2.push(Integer.toString(15));
        p2.push(Integer.toString(20));
        
        while (!p1.empty() && !p2.empty())
            paux.push(Integer.toString(Integer.parseInt(p1.pop())+ Integer.parseInt(p2.pop())));
        
        while(!paux.empty())
            p3.push(Integer.toString(Integer.parseInt(paux.pop())));
        
        while (!p3.empty())
            System.out.println("Dato :"+ Integer.parseInt(p3.pop()));
        
        System.out.println("Pilas sumadas");
        
        
        Stack <String>  p4 = new Stack <String>();
        Stack <String>  p5 = new Stack <String>();
        Stack <String>  paux1 = new Stack <String>();
        Stack <String>  p6 = new Stack <String>();
        
        p4.push(Integer.toString(3));
        p4.push(Integer.toString(4));
        p4.push(Integer.toString(30));
        p5.push(Integer.toString(10));
        p5.push(Integer.toString(50));
        p5.push(Integer.toString(20));
        
        while (!p4.empty() && !p5.empty())
            paux1.push(Integer.toString(Integer.parseInt(p4.pop())+ Integer.parseInt(p5.pop())));
        
        while(!paux1.empty())
            p6.push(Integer.toString(Integer.parseInt(paux1.pop())));
        
        while (!p6.empty())
            System.out.println("Dato :"+ Integer.parseInt(p6.pop()));
        
        System.out.println("Pilas restadas");
        
        
        }
        
        
        
    }
    
